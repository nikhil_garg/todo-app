﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using ToDo.Domain.Enums;

namespace ToDo.Domain.Entities
{
    public class UserTask
    {
        public Guid Id { get; set; }

        public int TaskId { get; set; }

        [Required]
        public Task Task { get; set; }

        public Guid UserId { get; set; }

        [Required]
        public User User { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime EndTime { get; set; }

        public Enumerations.TaskStatus Status { get; set; }
    }
}
