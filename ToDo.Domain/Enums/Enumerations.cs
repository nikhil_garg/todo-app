﻿namespace ToDo.Domain.Enums
{
    public class Enumerations
    {
        public enum TaskStatus
        {
            Pending,
            Invalid,
            Completed,
            Delete
        }
    }
}
