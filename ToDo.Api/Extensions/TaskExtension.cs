﻿using System;
using ToDo.Api.Requests;
using ToDo.Domain.Entities;
using ToDo.Domain.Enums;

namespace ToDo.Api.Extensions
{
    public static class TaskExtension
    {
        public static Task ToTask(this TaskParameter taskParameter)
        {
            return new Task
            {
                Id = new Random().Next(),
                Name = taskParameter.Name,
                Description = taskParameter.Description         
            };
        }

        public static UserTask ToUserTask(this TaskParameter taskParameter, Guid userId, int taskId)
        {
            return new UserTask
            {
                UserId = userId,
                TaskId = taskId,
                Status = Enumerations.TaskStatus.Pending,
                StartTime = taskParameter.StartTime,
                EndTime = taskParameter.EndTime
            };
        }
    }
}