﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToDo.Api.Requests
{
    public class TaskParameter
    {
        public Guid UserId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime EndTime { get; set; }
    }
}