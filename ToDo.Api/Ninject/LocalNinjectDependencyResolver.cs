﻿using System.Web.Http.Dependencies;
using Ninject;

namespace ToDo.Api.Ninject
{
    public class LocalNinjectDependencyResolver : LocalNinjectDependencyScope, IDependencyResolver
    {
        private IKernel _kernel;

        public LocalNinjectDependencyResolver(IKernel kernel)
            : base(kernel)
        {
            _kernel = kernel;
        }

        public IDependencyScope BeginScope()
        {
            return new LocalNinjectDependencyScope(_kernel.BeginBlock());
        }
    }
}