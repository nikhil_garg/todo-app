﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ToDo.Api.Extensions;
using ToDo.Api.Requests;
using ToDo.Domain.Entities;
using ToDo.Persistence.Context;
using ToDo.Persistence.UnitOfWork;

namespace ToDo.Api.Controllers
{
    public class TaskController : ApiController
    {
        // GET api/values
        public IEnumerable<Task> Get(Guid userId)
        {
            IEnumerable<Task> tasks;
            var dbContext = new ToDoContext();
            using (var uow = new UnitOfWork(dbContext))
            {
                var userTasks =
                    uow.UserTaskRepository.GetAll().Where(ut1 => ut1.UserId == userId).Select(ut2 => ut2.TaskId);
                tasks = uow.TaskRepository.GetAll().Where(t => userTasks.Contains(t.Id));
            }

            return tasks;
        }

        // POST api/values
        public void Post(TaskParameter taskParameter)
        {
            var dbContext = new ToDoContext();
            var task = taskParameter.ToTask();
            var userTask = taskParameter.ToUserTask(taskParameter.UserId, task.Id);

            using (var uow = new UnitOfWork(dbContext))
            {
                uow.TaskRepository.Add(task);
                uow.UserTaskRepository.Add(userTask);
            }
        }
    }
}
