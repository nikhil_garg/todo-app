﻿using System.Data.Entity;
using ToDo.Domain.Entities;

namespace ToDo.Persistence.Context
{
    public class ToDoContext : DbContext
    {
        public virtual IDbSet<User> Employees { get; set; }
        public virtual IDbSet<Task> Departments { get; set; }
        public virtual IDbSet<UserTask> Teams { get; set; }
    }
}

