﻿using System;
using System.Data.Entity;
using ToDo.Domain.Entities;
using ToDo.Persistence.Context;
using ToDo.Persistence.Repositories;

namespace ToDo.Persistence.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private readonly DbContext _context;
        private bool _disposed;
        private readonly Lazy<IRepository<User>> _userRepository;
        private readonly Lazy<IRepository<Task>> _taskRepository;
        private readonly Lazy<IRepository<UserTask>> _userTaskRepository;

        public IRepository<User> UserRepository
        {
            get
            {
                return _userRepository.Value;
            }
        }

        public IRepository<Task> TaskRepository
        {
            get
            {
                return _taskRepository.Value;
            }
        }

        public IRepository<UserTask> UserTaskRepository
        {
            get
            {
                return _userTaskRepository.Value;
            }
        }

        public UnitOfWork(ToDoContext context)
        {
            _context = context as DbContext;
            _userRepository = new Lazy<IRepository<User>>(() => new GenericRepository<User>(context));
            _taskRepository = new Lazy<IRepository<Task>>(() => new GenericRepository<Task>(context));
            _userTaskRepository = new Lazy<IRepository<UserTask>>(() => new GenericRepository<UserTask>(context));
        }

        public void CommitUnitOfWork()
        {
            _context.SaveChanges();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool isDisposing)
        {
            if (!_disposed)
            {
                if (isDisposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }
    }
}
