﻿namespace ToDo.Persistence.UnitOfWork
{
    public interface IUnitOfWork
    {
        void CommitUnitOfWork();
    }
}
