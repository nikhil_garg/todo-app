﻿using Ninject.Extensions.Conventions;
using Ninject.Modules;

namespace ToDo.Persistence.Ninject
{
    public class NinjectRegistry : NinjectModule
    {
        public override void Load()
        {
            System.Diagnostics.Debug.WriteLine(string.Format("### in {0}.Load", typeof(NinjectRegistry).FullName));

            Kernel.Bind(ctx => ctx
                .FromThisAssembly()
                .SelectAllTypes()
                .BindAllInterfaces());
        }
    }
}
