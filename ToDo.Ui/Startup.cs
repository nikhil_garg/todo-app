﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ToDo.Ui.Startup))]
namespace ToDo.Ui
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
